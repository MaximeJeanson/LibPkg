require 'mongo'

module LibPkg
  module DSL
    self.extend Rake::DSL

    private
    def artifact(config, dependencies)

      client = Mongo::Client.new('mongodb://127.0.0.1:27017/libpkg')
      desc "Generate #{config.artifact_name} libpkg"
      rule config.artifact_name => dependencies do |task|
        FileUtils::mkpath Pathname.new(task.name).parent

        Zip::File.open(task.name, Zip::File::CREATE) do |zipfile|
          task.sources.each do |filename|
            zipfile.add(filename.sub(config.base_dir + '/', ''), filename)
          end
        end
        if client.database.fs.find_one(:filename => config.artifact_uri).nil?
          file = File.open(config.artifact_name)
          grid_file = Mongo::Grid::File.new(file.read, :filename => config.artifact_uri, :chunk_size => 4096)
          file_id = client.database.fs.insert_one grid_file

          result = client[:libs].insert_one({
                                                name: config.name,
                                                version: config.version.to_s,
                                                namespace: config.namespace,
                                                file: file_id
                                            })
        end
      end
    end

    class ArtifactConfig
      attr_reader :name, :namespace, :version, :base_dir

      def initialize(base_dir, version, namespace, name)
        @name = name
        @namespace = namespace
        @version = version
        @base_dir = base_dir
      end

      def major
        @version.major
      end

      def minor
        @version.minor
      end

      def revision
        @version.revision
      end

      def build
        @version.build
      end

      def artifact_name
        "#{self.namespace.sub('.', '/')}/#{self.name}-#{self.version}.libpkg"
      end

      def artifact_uri
        "#{self.namespace}.#{self.name}-#{self.version}.libpkg"
      end
    end

    class Version
      attr_accessor :minor, :major, :revision, :build

      def initialize(major, minor, revision, build)
        @major = major
        @minor = minor
        @revision = revision
        @build = build
      end

      def to_s
        "#{major}.#{minor}.#{revision}-#{build}"
      end
    end
  end

  module ProjectFile
    # attr_reader :src, :libs

    @@src = 'localhost'
    @@libs = []

    def source(host)
      @@src = host
    end

    def lib(uri)
      @@libs << uri
    end

    def self.libs
      @@libs
    end

    def self.src
      @@src
    end
  end

  class Specification
    attr_accessor :authors, :name, :require_paths, :homepage, :files, :namespace, :email, :summary, :version

    def initialize(&block)
      @name = nil
      @namespace = nil
      @version = nil
      @authors = nil
      @email = nil
      @summary = nil
      @homepage = nil
      @files = nil
      @require_paths = nil

      block.call(self)
      # DSL::artifact(LibPkg::DSL::ArtifactConfig.new(require_paths, version, namespace, name), files)
    end
  end
end

self.extend LibPkg::DSL, LibPkg::ProjectFile
