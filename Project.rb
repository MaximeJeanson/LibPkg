require './lib_pkg'

include LibPkg::DSL

source 'localhost'

lib ArtifactConfig.new(nil, Version.new(1, 0, 0, 1), 'org.pie', 'test')
lib ArtifactConfig.new(nil, Version.new(1, 0, 1, 1), 'org.pie', 'test')